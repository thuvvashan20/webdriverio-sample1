class ActivityPage {

    get newButton(){
        return $('div.Button.Button--small.Button--primary.Omnibutton.Omnibutton--withoutSpreadsheetGrid.TopbarPageHeaderGlobalActions-omnibutton')
    }
    get taskButton(){
        return $('a.Omnibutton-dropdownButton.Omnibutton-addTask')
    }
    get taskName(){
        return $('.QuickAddTaskContents-nameInput')
    }
    get createTaskButton(){
        return $('.Button.Button--small.Button--primary.QuickAddTaskToolbar-createButton')
    }
    get searchField(){
        return  $('#topbar_search_input')
    }
    get editTask(){
        return $('.TaskName-input.override-focus-border')
    }
    get attachFile(){
        return $('.Icon.AttachVerticalIcon')
    }
    get getFromComputer(){
        return $('label=Your Computer')
    }
    get userProfile(){
        return $('.AvatarPhoto.AvatarPhoto--small.TopbarPageHeaderGlobalActions-settingsMenuAvatar.Avatar--color5')
    }
    get profile(){
        return $('span=My Profile Settings…')
    }
    get upload(){
        return $('input.AvatarInput-hiddenFileInput')
    }
    get subject(){
        return $('.HomeTeamLeadWelcomeMessage-description')
    }
    get saveChange(){
    return $('div.Button.Button--medium.Button--primary')
    }
    get removeLink(){
        return $('button.PhotoUpload-removeButton.LinkButton')
    }
    get toggleButtonOn(){
        return $('label.Switch-label')
    }
    get setLastDay(){
        return $('input.ValidatedTextInput-input.textInput.textInput--large.DateInput-textInput')
    }
    get setFirstDay(){
        return $('input.textInput.textInput--large.DateInput-textInput')
    }
    get tabClick(){
        return $$('a.tabView-tabName')
    }
    get ddlClick(){
        return $('#profile_settings_weekstartday_select')
    }
    get weekSelection(){
        return $$('span.MenuItem-label')
    }
    get checkboxSelection(){
        return $$('label.Checkbox-box.Checkbox-box--unchecked.Checkbox-box--enabled.Checkbox-box--primary')
    }
    get popupMessage(){
        return $('.ToastNotificationContent-text')
    }
    get fullName(){
        return $('.ValidatedTextInput-input.OnBlurValidatedTextInput-input.textInput.textInput--large')
    }
    get text(){
        return $('.HomeTeamLeadWelcomeMessage-header')
    }



    taskCreation(taskname1){
        this.newButton.click();
        this.taskButton.click();
        this.taskName.setValue(taskname1);
        browser.pause(3000);
        this.createTaskButton.click()
    }
    search(searchValue) {
        this.searchField.setValue(searchValue);
        browser.keys("\uE007");
        this.editTask.click();
        this.attachFile.click();
        /*    this.getFromComputer.click();
            browser.pause(2000);*/

        browser.pause(2000);
        const filePath = path.join(__dirname, '/package-lock.json');
        this.getFromComputer.fileUpload(filePath);

        /*   const filePath='/Users/thuvvareka/PhpstormProjects/React/untitled2/src/e2e/pages/package-lock.json';
           $('label=Your Computer').setValue(filePath);*/
        browser.pause(2000);
    }
    fileUpload1(){

        this.userProfile.click();
        this.profile.click();
        browser.pause(2000);

        const fileUpload = $('input[type="file"]');
        browser.execute(
            // assign style to elem in the browser
            (el) => el.style.display = 'block',
            // pass in element so we don't need to query it again in the browser
            fileUpload
        );
        browser.pause(3000);
        fileUpload.waitForDisplayed();
        const path = require('path');
        const filePath = path.join(__dirname, 'download.jpeg');
        fileUpload.setValue(filePath);
        browser.pause(5000);
        this.saveChange.click();
        this.text().getText();
        browser.pause(5000);

    }
    fileRemove(){
        this.userProfile.click();
        this.profile.click();
        this.removeLink.click();
        this.saveChange.click();
        browser.pause(2000);

    }
    dateSet(){
        this.userProfile.click();
        this.profile.click();
        this.toggleButtonOn.click();
     //
        this.setFirstDay.setValue('07/16/19');
        this.setLastDay.setValue('08/15/20');
        this.saveChange.click();
        browser.pause(5000);

    }
    dateSetRemove(){
        this.userProfile.click();
        this.profile.click();
        //
        this.setFirstDay.clearValue();
        this.setLastDay.clearValue();
        this.saveChange.click();
        browser.pause(5000);

    }
    dropdownSelection(){
        this.userProfile.click();
        this.profile.click();
        this.tabClick[4].click();
        this.ddlClick.click();
        this.weekSelection[2].click();
        this.checkboxSelection[0].click();
        // browser.$$('a.tabView-tabName')[4].click();
        browser.pause(5000);

        // this.tabClick[3].click();
    }
    checkBoxSelection(){

        this.userProfile.click();
        this.profile.click();
        this.tabClick[4].click();
        browser.pause(2000);

        this.checkboxSelection[0].click();
        this.checkboxSelection[1].click();
        this.checkboxSelection[2].click();
        browser.pause(5000);

    }
    handlePopup(taskName2){

        this.newButton.click();
        this.taskButton.click();
        this.taskName.setValue(taskName2);
        browser.pause(3000);
        this.createTaskButton.click();
        browser.pause(3000);
        const message=this.popupMessage.getText();
        console.log('******** '+ message);

    }
    clearAndSet(fulnameT){
        this.userProfile.click();
        this.profile.click();
        /*const value1=this.fullName.getValue();
        console.log('before deletion '+value1);
        this.fullName.clearValue();
        const value2=this.fullName().getValue();
        console.log('after deletion '+ value2);*/
    //    this.fullName.setValue(fulnameT);
        //this.fullName.click();
        this.fullName.clearValue(' ');
        this.fullName.setValue('bdsndm')
        browser.pause(3000);
    }
}
module.exports = ActivityPage;

